/* 1. forEach перебирає елементи масиву та приймає в себе функцію, яка виконуватиметься відносно кожного елементу по черзі.
 2. arr.length = 0
 3. Array.isArray(змінна або дані)
*/
const filterBy = (array, typeOfElement) => {
    const arrayFiltred = array.filter(element => typeof element !== typeOfElement);
    return arrayFiltred;
} 
// const newArray = [1, 2, "gbffed", "krhvgivkhnj", {vfd: "ffd", dfsd: "dff",}]
// console.log(filterBy(newArray, "object"))